/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Chuck Norris.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.ChuckDefendingMichelangelo
 * 
 * When defending against Darth Vader, this class should return HelperStrings.ChuckDefendingDarthVader
 * @author bricks
 *
 */
public class ChuckNorris {


}
